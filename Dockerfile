
FROM docker:latest

# 
# GSUTILS
# 

# RUN wget https://storage.googleapis.com/pub/gsutil.tar.gz && tar xfz gsutil.tar.gz -C $HOME && rm gsutil.tar.gz \
#     && echo 'export PATH=${PATH}:$HOME/gsutil' >> ~/.bashrc



# Google Cloud Tools 
    
# Install CA certs, openssl to https downloads, python for gcloud sdk  
RUN apk add --no-cache --update make ca-certificates openssl python curl gnupg openssh-client py-crcmod bash libc6-compat gnupg \
    # Download and install Google Cloud SDK
    && wget https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz \
    && tar zxvf google-cloud-sdk.tar.gz && ./google-cloud-sdk/install.sh --usage-reporting=false --path-update=true \
    && google-cloud-sdk/bin/gcloud --quiet components update \
    && rm google-cloud-sdk.tar.gz \
    && echo 'export PATH=${PATH}:/google-cloud-sdk/bin' >> ~/.bashrc



CMD [ "irb" ]
